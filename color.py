from tkinter import *
from tkinter import messagebox
from db import Database

db = Database('watercolor.db')


def select_item(event):
    try:
        global selected_item
        index = color_list.curselection()[0]
        selected_item = color_list.get(index)

        color_entry.delete(0, END)
        color_entry.insert(END, selected_item[1])
        brand_entry.delete(0, END)
        brand_entry.insert(END, selected_item[2])
        pigment_entry.delete(0, END)
        pigment_entry.insert(END, selected_item[3])
        lightfastness_entry.delete(0, END)
        lightfastness_entry.insert(END, selected_item[4])
    except IndexError:
        pass


def remove_item():
    db.remove(selected_item[0])
    clear_text()
    populate_list()


def add_item():
    if color_text.get() == "" or brand_text.get() == "" or pigment_text.get() == "" or lightfastness_text.get() == "":
        messagebox.showerror("Missing Required Fields",  "Please include all entries")
        return

    db.insert(color_text.get(), brand_text.get(), pigment_text.get(), lightfastness_text.get())
    color_list.delete(0, END)
    color_list.insert(END, (color_text.get(), brand_text.get(), pigment_text.get(), lightfastness_text.get()))
    clear_text()
    populate_list()


def clear_text():
    color_entry.delete(0, END)
    brand_entry.delete(0, END)
    pigment_entry.delete(0, END)
    lightfastness_entry.delete(0, END)


def update_item():
    db.update(selected_item[0], color_text.get(), brand_text.get(), pigment_text.get(), lightfastness_text.get())
    populate_list()


def populate_list():
    color_list.delete(0, END)
    for row in db.fetch():
        color_list.insert(END, row)


app = Tk()

color_text = StringVar()
color_label = Label(app, text='Color Name', font=('bold', 14), pady=20)
color_label.grid(row=0, column=0, sticky=W)
color_entry = Entry(app, textvariable=color_text)
color_entry.grid(row=0, column=1)

brand_text = StringVar()
brand_label = Label(app, text='Brand Name', font=('bold', 12))
brand_label.grid(row=0, column=2, sticky=W)
brand_entry = Entry(app, textvariable=brand_text)
brand_entry.grid(row=0, column=3)

pigment_text = StringVar()
pigment_label = Label(app, text='Pigment', font=('bold', 12))
pigment_label.grid(row=1, column=0, sticky=W)
pigment_entry = Entry(app, textvariable=pigment_text)
pigment_entry.grid(row=1, column=1)

lightfastness_text = StringVar()
lightfastness_label = Label(app, text='Lightfastness', font=('bold', 12))
lightfastness_label.grid(row=1, column=2, sticky=W)
lightfastness_entry = Entry(app, textvariable=lightfastness_text)
lightfastness_entry.grid(row=1, column=3)

color_list = Listbox(app, height=8, width=100, border=0)
color_list.grid(row=3, column=0, columnspan=3, rowspan=6, pady=20, padx=20)

scrollbar = Scrollbar(app)
scrollbar.grid(row=3, column=3)
color_list.configure(yscrollcommand=scrollbar.set)
scrollbar.configure(command=color_list.yview)
color_list.bind('<<ListboxSelect>>', select_item)

add_button = Button(app, text='Add', width=12, command=add_item)
add_button.grid(row=2, column=0)

remove_button = Button(app, text='Remove', width=12, command=remove_item)
remove_button.grid(row=2, column=1)

update_button = Button(app, text="Update", width=12, command=update_item)
update_button.grid(row=2, column=2)

clear_button = Button(app, text='Clear Input', width=12, command=clear_text)
clear_button.grid(row=2, column=3)

app.title('Watercolor Manager')
app.geometry('1000x500')

populate_list()

app.mainloop()