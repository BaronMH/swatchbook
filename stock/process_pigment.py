import re

f = open("pigments.txt", "r")
clean_expression = []

for line in f.readlines():
    new_line = line.split(",")
    for strip_line in new_line:
        clean_expression.append(strip_line.strip())

print(clean_expression)