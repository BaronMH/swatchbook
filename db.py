import sqlite3


class Database:
    def __init__(self, db):
        self.connection = sqlite3.connect(db)
        self.cursor = self.connection.cursor()
        self.cursor.execute("CREATE TABLE IF NOT EXISTS "
                            "watercolor("
                            "id INTEGER PRIMARY KEY, color text, brand text, pigment text, lightfastness text)")
        self.cursor.execute("CREATE TABLE IF NOT EXISTS "
                            "pigment("
                            "id INTEGER PRIMARY KEY, pigment")
        self.connection.commit()

    def populate_pigment(self, ):
        pass

    def fetch(self):
        self.cursor.execute("SELECT * from watercolor")
        rows = self.cursor.fetchall()
        return rows

    def insert(self, color, brand, pigment, lightfast):
        self.cursor.execute("INSERT INTO watercolor VALUES(NULL, ?, ?, ?, ?)", (color, brand, pigment, lightfast))
        self.connection.commit()

    def remove(self, id):
        self.cursor.execute("DELETE FROM watercolor WHERE id = ?", (id,))
        self.connection.commit()

    def update(self, id, color, brand, pigment, lightfast):
        self.cursor.execute("UPDATE watercolor SET color = ?, brand = ?, pigment = ?, lightfastness = ? WHERE id = ?",
                            (color, brand, pigment, lightfast, id))
        self.connection.commit()

    def __del__(self):
        self.connection.close()

# db = Database('watercolor.db')
# db.insert("Naples Yellow", "Daniel Smith", "PY35, PR101, PW4", "I - Excellent")
# db.insert("Mayan Red", "Turner", "PR254", "I - Excellent")
# db.insert("Cerulean Blue", "Winsor & Newton", "PB35", "I - Excellent")